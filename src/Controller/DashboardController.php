<?php


namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * Class DashboardController
 *
 * @package App\Controller
 * @Route("/dashboard")
 */
class DashboardController extends AbstractController
{
    private $client;

    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * @Route("/", name="app_dashboard_index")
     */
    public function index()
    {
        return $this->render('dashboard.html.twig');
    }

    /**
     * @Route("/users", name="app_users_list")
     * @param Request $request
     */
    public function usersList(Request $request) {
        $actionResult = '';
        $action = $request->get('action');
        if ($action) {
            if ($action === 'delete') {
                $id = $request->get('id');
                $actionResult = $this->deleteUser($id);
            }
            if ($action === 'update') {
                $id = $request->get('id');
                $actionResult = $this->updateUser($id, $request);
            }
            if ($action === 'insert') {
                $actionResult = $this->createUser($request);
            }
        }
        $url = $this->generateUrl(
            'api_users_list',
            [],
            UrlGeneratorInterface::ABSOLUTE_URL
        );
        $response = $this->prepareRequest('GET', $url);
        $decodedPayload = $response->toArray();

        $allUsers = $decodedPayload['body'];
        $message = $decodedPayload['message'];

        return $this->render('users/users.html.twig', array(
            'users' => $allUsers,
            'message' => $message,
            'actionResult' => $actionResult,
        ));
    }

    /**
     * @Route("/users/edit/{id}", name="app_user_edit")
     * @param Request $request
     * @param int $id
     */
    public function userEdit(Request $request, int $id) {
        $url = $this->generateUrl(
            'api_user_read',
            ['id' => $id],
            UrlGeneratorInterface::ABSOLUTE_URL
        );
        $response = $this->prepareRequest('GET', $url);
        $decodedPayload = $response->toArray();

        $user = $decodedPayload['body'];
        $message = $decodedPayload['message'];

        return $this->render('users/user_edit.html.twig', array(
            'user' => $user,
            'message' => $message
        ));
    }

    /**
     * @Route("/users/new", name="app_user_new")
     */
    public function userNew() {
        return $this->render('users/user_new.html.twig');
    }

    public function createUser(Request $request) {
        $user = [
            'firstName' => $request->get('firstName'),
            'lastName' => $request->get('lastName'),
            'email' => $request->get('email'),
            'password' => $request->get('password'),
        ];
        $url = $this->generateUrl(
            'api_user_new',
            [],
            UrlGeneratorInterface::ABSOLUTE_URL
        );
        $response = $this->prepareRequest('POST', $url, $user);
        $decodedPayload = $response->toArray();

        return $decodedPayload['message'];
    }

    public function updateUser($id, Request $request) {
        $user = [
            'id' => $id,
            'firstName' => $request->get('firstName'),
            'lastName' => $request->get('lastName'),
            'email' => $request->get('email'),
            'password' => $request->get('password'),
        ];
        $url = $this->generateUrl(
            'api_user_update',
            ['id' => $id],
            UrlGeneratorInterface::ABSOLUTE_URL
        );
        $response = $this->prepareRequest('PUT', $url, $user);
        $decodedPayload = $response->toArray();

        return $decodedPayload['message'];
    }

    public function deleteUser($id) {
        $url = $this->generateUrl(
            'api_user_delete',
            ['id' => $id],
            UrlGeneratorInterface::ABSOLUTE_URL
        );
        $response = $this->prepareRequest('DELETE', $url);
        $decodedPayload = $response->toArray();

        return $decodedPayload['message'];
    }

    function prepareRequest(string $method, string $url, $payload = null) {
        $options = [];
        if ($this->getUser()) {
            $options['headers'] = [
                'X-AUTH-TOKEN' => $this->getUser()->getApiKey()
            ];
        }
        if ($payload) {
            $options['json'] = $payload;
        }
        return $this->client->request($method, $url, $options);
    }
}
