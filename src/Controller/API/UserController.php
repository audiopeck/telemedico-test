<?php


namespace App\Controller\API;


use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class UserController
 *
 * @package App\Controller\API
 * @Route("/api/users")
 */
class UserController extends AbstractController
{
    /**
     * @Route("/", name="api_user_new", methods={"POST"})
     * @param Request $request
     *
     * @return Response
     */
    public function newUser(Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $data = json_decode($request->getContent(), true);
        $user = new User();
        $this->saveUser($user, $data, $passwordEncoder);

        return new JsonResponse([
            'message' => 'User Created',
            'body' => '',
        ]);
    }

    /**
     * @Route("/", name="api_users_list", methods={"GET"})
     * @param Request $request
     *
     * @return Response
     */
    public function getUsersList(Request $request): Response
    {
        $users = $this->getDoctrine()
            ->getRepository(User::class)
            ->getList();

        return new JsonResponse([
            'message' => 'Fetched users',
            'body' => $users,
        ]);
    }

    /**
     * @Route("/{id}", name="api_user_read", methods={"GET"})
     * @param string $id
     *
     * @return Response
     */
    public function getOneUser(string $id): Response
    {
        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->getById($id);

        return new JsonResponse([
            'message' => 'Fetched user',
            'body' => $user,
        ]);
    }

    /**
     * @Route("/{id}", name="api_user_update", methods={"PUT"})
     * @param Request $request
     * @param string $id
     *
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return Response
     */
    public function updateUser(Request $request, string $id, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $data = json_decode($request->getContent(), true);

        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->findOneBy([
                'id' => $id
            ]);

        $this->saveUser($user, $data, $passwordEncoder);

        return new JsonResponse([
            'message' => 'User Updated',
            'body' => '',
        ]);
    }

    /**
     * @Route("/{id}", name="api_user_delete", methods={"DELETE"})
     * @param string $id
     *
     * @return Response
     */
    public function deleteUser(string $id): Response
    {
        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->findOneBy([
                'id' => $id
            ]);

        if ($user) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($user);
            $em->flush();
        }

        return new JsonResponse([
            'message' => 'User deleted',
            'body' => '',
        ]);
    }

    function saveUser(User $user, $userData, UserPasswordEncoderInterface $passwordEncoder) {
        $user
            ->setFirstName($userData['firstName'])
            ->setLastName($userData['lastName'])
            ->setEmail($userData['email']);

        $plainPassword = $userData['password'];
        if ($userData['password']) {
            // encode the plain password
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $plainPassword
                )
            );
        }
        if (!$user->getApiKey()) {
            // create API key
            $apiKey = md5($user->getEmail().$plainPassword);
            $user->setApiKey($apiKey);
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();
    }
}
